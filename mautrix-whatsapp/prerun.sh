#!/bin/sh


mkdir /data


ln -s /synapse/config/homeserver.yaml /data/config.yaml
ln -s /synapse/secrets/config.yaml /data/registration.yaml

ls -la /data
cat /data/config.yaml
cat /data/registration.yaml

/docker-run.sh --no-update


